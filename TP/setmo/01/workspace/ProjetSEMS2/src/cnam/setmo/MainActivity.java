package cnam.setmo;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override // on demande au compilateru de verifier que cette classe existe
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Equivalente a this.setContentView(R.layout.activity_main);        
    }
}