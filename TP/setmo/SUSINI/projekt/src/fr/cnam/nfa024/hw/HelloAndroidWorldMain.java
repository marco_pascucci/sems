package fr.cnam.nfa024.hw;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class HelloAndroidWorldMain extends Activity
{

// CODICE DEI CAPTEURS
//    /** Called when the activity is first created. */
//
//    
//    @Override
//    public void onCreate(Bundle savedInstanceState)
//    {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.main);
//        
//        SensorManager sm = (SensorManager)this.getSystemService(SENSOR_SERVICE);
//        List<Sensor> ls = sm.getSensorList(Sensor.TYPE_ALL);
//        
//        for (int i = 0; i<ls.size(); i++) {
//	    Log.d(APP_TAG, ls.get(i).getName());
//        }
//    }
//    
//    @Override
//    public void onResume()
//    {
//	super.onResume();
//	SensorManager sm = (SensorManager)this.getSystemService(SENSOR_SERVICE);
//	SensorEventListener acceleroListener = new SensorEventListener() {
//	  public void onSensorChanged(SensorEvent evt){
//	      Log.d(APP_TAG,
//	      "gx = " + evt.values[0] + "m/s^2" +
//	      "gy = " + evt.values[1] + "m/s^2" +
//	      "gz = " + evt.values[2] + "m/s^2");
//	  }
//	  public void onAccuracyChanged(Sensor s, int acc){}
//	};
//	
//	Sensor accelero = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
//	sm.registerListener(acceleroListener, accelero, SensorManager.SENSOR_DELAY_NORMAL);
//    }
  
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.main);
      
      Log.d(LogCatTag.APP_TAG,"on create");
	  
	  Intent i = new Intent(this,MyService.class);
	  startService(i);
	  
  }
}
