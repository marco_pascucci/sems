package fr.cnam.nfa024.hw;

import android.app.Service;
//import android.net.ConnectivityManager;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service
{
	public boolean isRunning = false;
	MediaPlayer player;
	
    @Override
    public void onCreate()
    {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.internationale);
        player.setLooping(true); // Set looping
        player.setVolume(100,100);
        isRunning = true;
        Log.d(LogCatTag.APP_TAG,"service: on Create");
    }
    
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        //Music
        player.stop();
        player.release();
        isRunning = false;
    }
    
	@Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
    	Log.d(LogCatTag.APP_TAG,"service: on Start Command");
    	player.start();
    	return Service.START_STICKY;
    } 
    
    @Override
    public IBinder onBind(Intent i)
    {
    	return null;
    }
    
}