package com.example.olivier.monservice;

/**
 * Created by olivier on 29/11/2015.
 */

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

public class HelloService extends Service {

    private static final String TAG = "HelloService";

    private boolean isRunning  = false;
    MediaPlayer player;

    @Override
    public void onCreate() {
        super.onCreate();
        //SI DU SON
        player = MediaPlayer.create(this, R.raw.internationale);
        player.setLooping(true); // Set looping
        player.setVolume(100,100);

        Log.i(TAG, "Service onCreate");
        isRunning = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();

        Log.i(TAG, "Service onStartCommand");

        //Creating new thread for my service
        //Always write your long running tasks in a separate thread, to avoid ANR
        /*
        new Thread(new Runnable() {
            @Override
            public void run() {

                //Your logic that service will perform will be placed here
                //In this example we are just looping and waits for 1000 milliseconds in each loop.
                for (int i = 0; i < 5; i++) {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                    }

                    if(isRunning){
                        Log.i(TAG, "Service running");
                    }
                }

                //Stop service once it finishes its task
                stopSelf();
            }
        }).start();
*/
        return Service.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent arg0) {

        Log.i(TAG, "Service onBind");
        return null;
    }

    @Override
    public void onDestroy() {
        //Music
        player.stop();
        player.release();


        isRunning = false;
        Log.i(TAG, "Service onDestroy");
    }
}
