package cnam.SETMO;

import java.util.Date;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity implements View.OnClickListener {
	private Button btn;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn=(Button)findViewById(R.id.button1);
        btn.setOnClickListener(this);
        updateTime();
    }
    public void onClick(View view) {
    	updateTime();
    	}
    private void updateTime() {
    	btn.setText(new Date().toString());
    }
}
