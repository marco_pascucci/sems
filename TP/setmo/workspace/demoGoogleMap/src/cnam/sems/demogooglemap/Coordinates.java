package cnam.sems.demogooglemap;

import com.google.android.gms.maps.model.LatLng;

public interface Coordinates {
	public static final LatLng PARIS_CHAMPS_ELYSEES = new LatLng(48.870209, 2.306268);
}
