package cnam.sems.mycontacts;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import cnam.sems.mycontacts.MesConstantes;

public class LeDatabaseHelper extends SQLiteOpenHelper {

	public LeDatabaseHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(MesConstantes.REQUETE_CREATION_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("drop table " + MesConstantes.TABLE_CONTACTS + ";");
		onCreate(db);
	}

}
