package cnam.sems.mycontacts;

public interface MesConstantes {
	// All Static variables
	// Database Version
	public static final int DATABASE_VERSION = 1;
	// Database Name
	public static final String DATABASE_NAME = "contactsManager";
	// Contacts table name
	public static final String TABLE_CONTACTS = "contacts";
	// Contacts Table Columns names
	public static final String KEY_ID = "id";
	public static final String KEY_NAME = "name";
	public static final String KEY_PH_NO = "phone_number";
	public static final String REQUETE_CREATION_TABLE = "create table " + TABLE_CONTACTS + " ("
			+ KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_NAME + " text not null, "
			+ KEY_PH_NO + " text not null);";
}
