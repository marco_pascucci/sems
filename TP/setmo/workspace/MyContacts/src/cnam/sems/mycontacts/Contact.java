package cnam.sems.mycontacts;

public class Contact {
	// modellizza un contatto
	private int id;
	private String nom;
	private String numTelephone;
	
	// constructor
	public Contact(String nom, String tel) {
		this.setNom(nom);
		this.setNumTelephone(tel);
	}
	public Contact() {
	}
	
	// getter and setter methods
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getNumTelephone() {
		return numTelephone;
	}
	public void setNumTelephone(String numTelephone) {
		this.numTelephone = numTelephone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}

