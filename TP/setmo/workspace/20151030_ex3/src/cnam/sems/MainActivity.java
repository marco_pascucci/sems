package cnam.sems;

import android.app.ListActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends ListActivity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// get the resurces of this Acrivity
		Resources res = getResources();

		// set the Adapter for this list
		String[] CHOICES = new String[] { res.getString(R.string.see_all_stations), res.getString(R.string.about_us) };
		setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, CHOICES));

		// get the reference to the ListView in this activity
		ListView lv = getListView();

		// uncomment this fot searching when typing
		// lv.setTextFilterEnabled(true);

		// Listener
		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// le code lancé lors de la sélection d'un item
				Log.v("MARCO", "clicked position: " + position);
				// Toast.makeText(getApplicationContext(), "clicked position: "
				// + position + ", id: " + id, Toast.LENGTH_SHORT).show();
				String act;
				Intent i;

				switch (position) {

				case 1:
					// change screen by calling a new Activity with an Intent
					i = new Intent(getApplicationContext(), AboutUs.class);
					break;

				default:
					i = new Intent(getApplicationContext(), ListDesStations.class);
					break;

				}

				// i.putExtra("latitude", latitudeDuPointCourant);
				// i.putExtra("longitude", longitudeDuPointCourant);

				startActivity(i);
			}

		});
	}
}
