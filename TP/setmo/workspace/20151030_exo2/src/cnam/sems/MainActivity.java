package cnam.sems;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);		   
	}
	
	
	private void auth() {
		// check ID and Password
		EditText idEditText  = (EditText) findViewById(R.id.editText1);
		EditText pwdEditText  = (EditText) findViewById(R.id.editText2);
		String id = idEditText.getText().toString();
		String pwd = pwdEditText.getText().toString();
		Log.v("MARCO", "entered ID: " + id);
		Log.v("MARCO", "entered PWD: " + pwd);
		if ( id.equals("marco") && pwd.equals("pass") ) {
			Log.v("MARCO", "auth OK");
			Toast.makeText(getApplicationContext(), "Log-in successful!", Toast.LENGTH_LONG).show();
			Intent i = new Intent(getApplicationContext(), LoginSuccessActivity.class);
			i.putExtra("id", id);
			startActivity(i);
		} else {
			Log.v("MARCO", "auth ERROR");
			Toast.makeText(getApplicationContext(), "Wrong Log-in!", Toast.LENGTH_LONG).show();
		}
	}
	
	public void onClickButton(View w){
		Log.v("MARCO", "Une nouvelle partie ?");
		//marco	Toast.makeText(getApplicationContext(), "Toast is ready!", Toast.LENGTH_LONG).show();
		auth();
	}

}
