package cnam.sems;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class LoginSuccessActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_success);
		String idRecupere = getIntent().getExtras().getString("id");
		TextView tv = (TextView) findViewById(R.id.tv);
		tv.setText(idRecupere);
	}
}
