package jmf.sharedprefs;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

public class SharedPreferencesJMFProjetActivity extends Activity {
	private Button btIncrementer;
	private Button btDecrementer;
	private CheckBox caseSauvegarde;
	private EditText zoneTexte;
	private SharedPreferences mesPrefs;

	private static final String NOM_PREFS = "JMFPrefs";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		Log.i("JMF","Entr�e de onCreate()");

		btIncrementer = (Button)findViewById(R.id.button1);
		btDecrementer = (Button)findViewById(R.id.button2);
		caseSauvegarde = (CheckBox)findViewById(R.id.checkBox1);
		zoneTexte = (EditText)findViewById(R.id.editText1);

		btIncrementer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				zoneTexte.setText("" + (Integer.parseInt(zoneTexte.getText().toString()) + 1));				
			}
		});       

		btDecrementer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				zoneTexte.setText("" + (Integer.parseInt(zoneTexte.getText().toString()) - 1));				
			}
		});
		caseSauvegarde.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				sauvegardeEtatCaseACocher();				
			}
		});
	}

	private void sauvegardeEtatCaseACocher() {
		boolean etatCaseACocher = caseSauvegarde.isChecked();
		mesPrefs = getSharedPreferences(NOM_PREFS, Context.MODE_PRIVATE);
		Editor unEditeurDesSharedPrefs = mesPrefs.edit();
		unEditeurDesSharedPrefs.putBoolean("caseCoche", etatCaseACocher);
		unEditeurDesSharedPrefs.commit();

	}
	
	private void initialiseCaseACocher() {
		mesPrefs = getSharedPreferences(NOM_PREFS, Context.MODE_PRIVATE);
		boolean estCoche = mesPrefs.getBoolean("caseCoche", false);
		caseSauvegarde.setChecked(estCoche);   
	}	
	
	private void initialiseZoneTexte() {
		Log.i("JMF","Entr�e de initialiseZoneTexte()");
		mesPrefs = getSharedPreferences(NOM_PREFS, Context.MODE_PRIVATE);
		int leCompteur = mesPrefs.getInt("compteur", 0);
		zoneTexte.setText("" + leCompteur);        
	}

	private void sauvegardeZoneTexte() {
		int leCompteur = Integer.parseInt(zoneTexte.getText().toString());
		mesPrefs = getSharedPreferences(NOM_PREFS, Context.MODE_PRIVATE);
		Editor unEditeurDesSharedPrefs = mesPrefs.edit();
		unEditeurDesSharedPrefs.putInt("compteur", leCompteur);
		unEditeurDesSharedPrefs.commit();
	}


	@Override
	public void onStop() {
		super.onStop();
		Log.i("JMF","Entr�e de onStop()");
		sauvegardeZoneTexte();
		sauvegardeEtatCaseACocher();
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.i("JMF","Entr�e de onResume()");
		initialiseZoneTexte();
		initialiseCaseACocher();
	}
}