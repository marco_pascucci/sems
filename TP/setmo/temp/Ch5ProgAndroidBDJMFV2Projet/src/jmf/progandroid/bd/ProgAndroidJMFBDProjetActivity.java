package jmf.progandroid.bd;

import java.util.ArrayList;

import jmf.progandroid.bd.R;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ProgAndroidJMFBDProjetActivity extends Activity {
	private PlanetesDB_DAO dbDAO;
	private ArrayList<Planete> planetes;
	private static final int DIALOG_ITEM = 1;
	private int selectionListe;
	private ListView liste;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i("JMF","debut de  onCreate() de PlanetesActivite");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		// Cr�e une instance de notre DAO
		dbDAO = new PlanetesDB_DAO(this);
		// Ouvre la base de donn�es
		dbDAO.open();

		// Interaction avec le bouton d'alimentation de la base de donn�es
		Button boutonAlimBase = (Button) findViewById(R.id.bouton_alimente_base_donnees);
		boutonAlimBase.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				alimenterBase();
			}
		});
		
		// Interaction avec le bouton de vidage de la base de donn�es
		Button boutonVideBase = (Button) findViewById(R.id.bouton_vide_base_donnees);
		boutonVideBase.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				videBase();
			}
		});

		// Alimentation de la liste des planetes en bases de donn�es
		alimenterListePlanetes();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// Fermeture de la base.
		dbDAO.close();
	}

//	@Override
//	protected Dialog onCreateDialog(int id) {
//		switch (id) {
//		case DIALOG_ITEM:
//			Dialog dialogItem = new Dialog(this);
//			Planete planete = planetes.get(selectionListe);
//			dialogItem.setTitle(planete.getNom());
//			TextView txtContent = new TextView(this);
//			txtContent.setText(planete.getNom() + "\nRayon : "
//					+ planete.getRayon());
//			txtContent.setPadding(10, 10, 10, 10);
//			dialogItem.setContentView(txtContent);
//			return dialogItem;
//		}
//		return null;
//	}
//
//	@Override
//	protected void onPrepareDialog(int id, Dialog dialog) {
//		switch (id) {
//		case DIALOG_ITEM:
//			Planete planete = planetes.get(selectionListe);
//			dialog.setTitle(planete.getNom());
//			TextView txtContent = new TextView(this);
//			txtContent.setText(planete.getNom() + "\nRayon : "
//					+ planete.getRayon());
//			txtContent.setPadding(10, 10, 10, 10);
//			dialog.setContentView(txtContent);
//			break;
//		}
//	}

	private void alimenterListePlanetes() {
		Log.i("JMF", "Entree de alimenterListePlanetes()");
		// R�cup�re les donn�es
		planetes = dbDAO.getAllPlanetes();
		for (Planete pl : planetes) {
			Log.i("JMF", pl.toString());
		}

		// Met � jour l'interface utilisateur
		liste = (ListView) findViewById(R.id.list_planetes);
		ArrayAdapter<Planete> planetesAdapter = new ArrayAdapter<Planete>(this,
				android.R.layout.simple_list_item_1, planetes);
		liste.setAdapter(planetesAdapter);
		liste.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				selectionListe = arg2;
//				showDialog(DIALOG_ITEM);
			}

		});
	}

	/**
	 * Alimente la base de donn�es SQLite avec quelques plan�tes.
	 */
	private void alimenterBase() {
		Log.i("JMF", "Dans alimenterBase()");
		Planete terre = new Planete("Terre", 1.0f);
		Planete mars = new Planete("Mars", 1.0f);
		Planete mercure = new Planete("Mercure", 1.0f);
		dbDAO.insertPlanete(terre);
		dbDAO.insertPlanete(mars);
		dbDAO.insertPlanete(mercure);
		planetes.add(terre);
		planetes.add(mars);
		planetes.add(mercure);
		ArrayAdapter<Planete> planetesAdapter = new ArrayAdapter<Planete>(this,
				android.R.layout.simple_list_item_1, planetes);
		liste.setAdapter(planetesAdapter);
	}	
	
	/**
	 * Vide la base de donn�es SQLite
	 */
	private void videBase() {
		Log.i("JMF", "Dans videBase()");
		dbDAO.videLaBase();
		planetes.clear();
		ArrayAdapter<Planete> planetesAdapter = new ArrayAdapter<Planete>(this,
				android.R.layout.simple_list_item_1, planetes);
		liste.setAdapter(planetesAdapter);		
	}
}