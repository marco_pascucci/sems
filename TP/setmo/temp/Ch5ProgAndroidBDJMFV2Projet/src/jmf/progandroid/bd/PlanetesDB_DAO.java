package jmf.progandroid.bd;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class PlanetesDB_DAO {
	private static final int BASE_VERSION = 1;
	private static final String BASE_NOM = "planetes.db";
	private static final String TABLE_PLANETES = "table_planetes";
	public static final String COLONNE_ID = "id";
	public static final String COLONNE_NOM = "nom";
	public static final String COLONNE_RAYON = "rayon";

	/**
	 * La requ�te de cr�ation de la structure de la base de donn�es.
	 */
	private static final String REQUETE_CREATION_BD = "create table "
			+ TABLE_PLANETES + " (" + COLONNE_ID
			+ " integer primary key autoincrement, " + COLONNE_NOM
			+ " text not null, " + COLONNE_RAYON + " text not null);";

	/**
	 * L'instance de la base qui sera manipul�e au travers de cette classe
	 */
	private SQLiteDatabase maBaseDonnees;

	/**
	 * La classe d'aide pour cette base de donn�es.
	 */	
	private MaBaseOpenHelper baseHelper;

	public PlanetesDB_DAO(Context ctx) {
		baseHelper = new MaBaseOpenHelper(ctx, BASE_NOM, null, BASE_VERSION);
	}

	/**
	 * Ouvre la base de donn�es en �criture.
	 * 
	 * @return Une instance de la base ouverte.
	 */
	public SQLiteDatabase open() {
		maBaseDonnees = baseHelper.getWritableDatabase();
		return maBaseDonnees;
	}

	/**
	 * Ferme la base de donn�es.
	 */
	public void close() {
		maBaseDonnees.close();
	}

	/**
	 * R�cup�re une plan�te en fonction de son nom.
	 * 
	 * @param nom
	 *            Le nom de la plan�te � retourner.
	 * @return La plan�te dont le nom est �gale au param�tre 'nom'.
	 */
	public Planete getPlanete(String nom) {
//		Cursor c = maBaseDonnees.query(TABLE_PLANETES, new String[] {
//				COLONNE_ID, COLONNE_NOM, COLONNE_RAYON }, null, null, null,
//				COLONNE_NOM + " LIKE " + nom, null);
		String laRequeteSQL = "SELECT " + COLONNE_ID + ", " + COLONNE_NOM + ", " + COLONNE_RAYON
				+ " FROM " + TABLE_PLANETES + " WHERE " + COLONNE_NOM + " LIKE " + nom ;
		Cursor c = maBaseDonnees.rawQuery(laRequeteSQL, null);
		
		return cursorToPlanete(c);
	}

	/**
	 * R�cup�re une plan�te en fonction de son id.
	 * 
	 * @param id
	 *            L'identifiant de la plan�te � retourner.
	 * @return La plan�te poss�dant l'identifiant �gale au param�tre 'id'.
	 */
	public Planete getPlanete(int id) {
		Cursor c = maBaseDonnees.query(TABLE_PLANETES, new String[] {
				COLONNE_ID, COLONNE_NOM, COLONNE_RAYON }, null, null, null,
				COLONNE_ID + " = " + id, null);
		return cursorToPlanete(c);
	}

	/**
	 * R�cup�re une plan�te en fonction de son id et retourne le r�sultat sous
	 * forme d'un curseur.
	 * 
	 * @param id
	 *            L'identifiant de la plan�te � retourner.
	 * @return Le curseur contenant la plan�te.
	 */
	public Cursor getPlaneteCurseur(int id) {
		return maBaseDonnees.query(TABLE_PLANETES, new String[] { COLONNE_ID,
				COLONNE_NOM, COLONNE_RAYON }, null, null, null, COLONNE_ID
				+ " = " + id, null);
	}

	/**
	 * Retourne toutes les plan�tes de la base de donn�es.
	 * 
	 * @return Un curseur contenant toutes les plan�tes de la base de donn�es.
	 */
	public ArrayList<Planete> getAllPlanetes() {
		Cursor c = maBaseDonnees.query(TABLE_PLANETES, new String[] {
				COLONNE_ID, COLONNE_NOM, COLONNE_RAYON }, null, null, null,
				null, null);
		return cursorToPlanetes(c);
	}

	/**
	 * Retourne toutes les plan�tes de la base de donn�es.
	 * 
	 * @return Un curseur contenant toutes les plan�tes de la base de donn�es.
	 */
	public Cursor getAllPlanetesCurseur() {
		return maBaseDonnees.query(TABLE_PLANETES, new String[] {COLONNE_ID,
				COLONNE_NOM, COLONNE_RAYON }, null, null, null, null, null);
	}
	
	public SQLiteDatabase getBaseDonnees() {
		return maBaseDonnees;
	}

	/**
	 * Transforme un cursor en objet de type 'Planete'.
	 * 
	 * @param c
	 *            Le curseur � utilis� pour r�cup�rer les donn�es de la plan�te.
	 * @return Une instance d'une plan�te avec les valeurs du curseur.
	 */
	private Planete cursorToPlanete(Cursor c) {
		// Si la requ�te ne renvoie pas de r�sultat
		if (c.getCount() == 0) {
			c.close();
			return null;
		}

		Planete retPlanete = new Planete();
		// Extraction des valeurs depuis le curseur
		retPlanete.setId(c.getInt(c.getColumnIndex(COLONNE_ID)));
		retPlanete.setNom(c.getString(c.getColumnIndex(COLONNE_NOM)));
		retPlanete.setRayon(c.getFloat(c.getColumnIndex(COLONNE_RAYON)));
		// Ferme le curseur pour lib�rer les ressources
		c.close();
		return retPlanete;
	}

	private ArrayList<Planete> cursorToPlanetes(Cursor c) {
		// Si la requ�te ne renvoie pas de r�sultat
		if (c.getCount() == 0){
			c.close();
			return new ArrayList<Planete>(0);
		}

		ArrayList<Planete> retPlanetes = new ArrayList<Planete>(c.getCount());
		c.moveToFirst();
		do {
			Planete planete = new Planete();
			planete.setId(c.getInt(c.getColumnIndex(COLONNE_ID)));
			planete.setNom(c.getString(c.getColumnIndex(COLONNE_NOM)));
			planete.setRayon(c.getFloat(c.getColumnIndex(COLONNE_RAYON)));
			retPlanetes.add(planete);
		} while (c.moveToNext());
		// Ferme le curseur pour lib�rer les ressources
		c.close();
		return retPlanetes;
	}

	/**
	 * Ins�re une plan�te dans la table des plan�tes.
	 * 
	 * @param planete
	 *            La plan�te � ins�rer.
	 */
	public long insertPlanete(Planete planete) {
		ContentValues valeurs = new ContentValues();
		valeurs.put(COLONNE_NOM, planete.getNom());
		valeurs.put(COLONNE_RAYON, planete.getRayon());
		return maBaseDonnees.insert(TABLE_PLANETES, null, valeurs);
	}

	/**
	 * Ins�re une plan�te dans la table des plan�tes.
	 * 
	 * @param valeurs
	 *            Les valeurs � ins�rer dans la table des plan�tes.
	 */
	public long insertPlanete(ContentValues valeurs) {
		return maBaseDonnees.insert(TABLE_PLANETES, null, valeurs);
	}

	/**
	 * Met � jour la plan�te poss�dant l'index sp�cifi�.
	 */
	public int updatePlanete(int id, Planete planeteToUpdate) {
		ContentValues valeurs = new ContentValues();
		valeurs.put(COLONNE_NOM, planeteToUpdate.getNom());
		valeurs.put(COLONNE_RAYON, planeteToUpdate.getRayon());
		return maBaseDonnees.update(TABLE_PLANETES, valeurs, COLONNE_ID + " = "
				+ id, null);
	}

	/**
	 * Met � jour la ou les plan�tes en fonction des conditions sp�cifi�es.
	 */
	public int updatePlanete(ContentValues valeurs, String where,
			String[] whereArgs) {
		return maBaseDonnees.update(TABLE_PLANETES, valeurs, where, whereArgs);
	}

	/**
	 * Supprime la ou les plan�tes poss�dant le nom sp�cifi�.
	 * 
	 * @param nom
	 *            Le nom des plan�tes � supprimer de la base.
	 */
	public int removePlanete(String nom) {
		return maBaseDonnees.delete(TABLE_PLANETES, COLONNE_NOM + " LIKE "
				+ nom, null);
	}

	/**
	 * Supprime la ou les plan�tes poss�dant l'id sp�cifi�.
	 * 
	 * @param id
	 *            L'id de la plan�te � supprimer de la base.
	 */
	public int removePlanete(int id) {
		return maBaseDonnees.delete(TABLE_PLANETES, COLONNE_ID + " = " + id,
				null);
	}

	/**
	 * Supprime la ou les plan�tes de la base de donn�es correspondant aux
	 * crit�res.
	 */
	public int removePlanete(String where, String[] whereArgs) {
		return maBaseDonnees.delete(TABLE_PLANETES, where, whereArgs);
	}
	
	public void videLaBase() {
		// Dans notre cas, nous supprimons la base et les donn�es pour en
		// cr�er une nouvelle ensuite. Vous pouvez cr�er une logique de mise
		// � jour propre � votre base permettant de garder les donn�es � la
		// place.
		maBaseDonnees.execSQL("drop table " + TABLE_PLANETES + ";");
		// Cr�ation de la nouvelle structure.
		maBaseDonnees.execSQL(REQUETE_CREATION_BD);
		Log.i("JMF", "Fin videLaBase() de PlanetesDBAdaptateur");
	}

	/**
	 * Classe d'aide pour la cr�ation et la mise � jour de la base de donn�es.
	 */
	private class MaBaseOpenHelper extends SQLiteOpenHelper {

		public MaBaseOpenHelper(Context context, String nom,
				CursorFactory cursorfactory, int version) {
			super(context, nom, cursorfactory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(REQUETE_CREATION_BD);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Dans notre cas, nous supprimons la base et les donn�es pour en
			// cr�er une nouvelle ensuite. Vous pouvez cr�er une logique de mise
			// � jour propre � votre base permettant de garder les donn�es � la
			// place.
			db.execSQL("drop table" + TABLE_PLANETES + ";");
			// Cr�ation de la nouvelle structure.
			onCreate(db);
		}
	}
}