package jmf.progandroid.bd;

public class Planete {
	private int id;	
	private String nom;	
	private float rayon;
	
	public Planete() { }
	
	public Planete(String nom, float rayon)
	{
		this.nom = nom;
		this.rayon = rayon;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public float getRayon() {
		return rayon;
	}

	public void setRayon(float rayon) {
		this.rayon = rayon;
	}

	@Override
	public String toString() {
		return nom;
	}
}
