package com.eyrolles.android.fichier;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class FichierActivite extends Activity {

	private static final String NOM_FICHIER = "MonFichier.dat";
	private Button bt;
	private EditText et;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		et = (EditText) findViewById(R.id.editText1);
		bt = (Button)findViewById(R.id.button1);
		bt.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				sauvegarder();			
			}
		});
	}

	@Override
	public void onStop() {
		super.onStop();
		sauvegarder();

	}
	
	private void sauvegarder() {
		FileOutputStream ous;
		try {
			ous = openFileOutput(NOM_FICHIER, MODE_PRIVATE);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(ous));
			String stASauvegarder = et.getText().toString();;
			Log.i("JMF","contenu du composant EditText : " + stASauvegarder);
			bw.write(stASauvegarder);
			bw.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.i("JMF", "Pb dans onStop()");
		}
		// Log.i("JMF", "le fichier " + NOM_FICHIER + " se trouve � " + getFilesDir().getAbsolutePath());

	}

	public void onResume() {  
		try {
			super.onResume();
			FileInputStream ins = openFileInput(NOM_FICHIER);
			BufferedReader br = new BufferedReader(new InputStreamReader(ins));
			String contenuARetablir = "";
			String valeurLue = br.readLine();
			while (valeurLue != null){
				contenuARetablir += valeurLue + "\n";
				valeurLue = br.readLine();
			}
			// et.setText(contenuARetablir);
			et.setText(contenuARetablir + "\n\n le fichier " + NOM_FICHIER + " se trouve � " + getFilesDir().getAbsolutePath());
			ins.close();
		} catch (Exception e) {
			e.printStackTrace();
			Log.i("JMF", "Pb dans onResume()");   
		}	
	}
}