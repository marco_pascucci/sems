#ifndef COPYFILE_H_
#define COPYFILE_H_
int verify_args(int argc, char *argv[]);
int copy_file(char *inputfile, char *outputfile);
#endif
