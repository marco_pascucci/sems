#include <stdio.h>
#include <stdlib.h>

#define LINE_LENGTH 128

int copy_file(char *inputfile, char *outputfile){
	FILE *in_file = NULL;
	FILE *out_file = NULL;
    char str[LINE_LENGTH];

//	printf("in: %s\n", inputfile);
//	printf("out: %s\n", outputfile);
	
	// stop if error opening files
    in_file = fopen(inputfile,"r");
	if ( in_file == NULL ) {
		printf("can't open file %s\n", inputfile);
		return 1;
	}
    
    out_file = fopen(outputfile,"w");
	if ( out_file == NULL){
		printf("ERROR opening file %s\n", outputfile);
		return 1;
	}
    
    while((fgets(str,60,in_file)) != NULL) {
//        printf("%s", str);
        fputs(str,out_file);
        
//        if (fputs(str,out_file) == EOF) {
//            printf("EOF\n");
//            break;
//        }
	}


//	fclose(in_file);
//	fclose(out_file);
	return 0;
}
