#include "verifyargs.h"
#include "docopy.h"

/* copy one file into another */

int main(int argc, char *argv[]) {
    if (!verify_args(argc, argv)){
        copy_file(argv[1],argv[2]);
    } else {
        return -1;
    }
}