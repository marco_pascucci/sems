#include <stdio.h>
#include "verifyargs.h"

/* check if there are 2 command line arguments */

int verify_args(int argc, char *argv[]){
	if (argc >=3){
		return 0;
	} else {
		printf("expected 2 args, got %d",argc-1);
		return 1;
	}
}
