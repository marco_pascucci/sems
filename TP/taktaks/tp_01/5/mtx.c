#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
int counter = 0;

void *looping(void *arg){
    int i;
    for (i=0; i<5; i++) {
		sleep(1);
		pthread_mutex_lock( &mtx );
			counter++;
	    printf("%d, %s, counter: %d\n",i, (char *) arg,counter);
		pthread_mutex_unlock( &mtx );
    }
}

void *action(void *arg){
    printf("action\n");
    return 0;
}


int main() {
	
	const char *message0 = "Thread 0";
	const char *message1 = "Thread 1";

    pthread_t th0 = NULL;
    pthread_t th1 = NULL;
	pthread_mutex_init(&mtx, NULL);

    pthread_create(&th0, NULL, looping, (void *) message0);
    pthread_create(&th1, NULL, looping, (void *) message1);

    pthread_join(th0, NULL);
    pthread_join(th1, NULL);

    return 0;
}
