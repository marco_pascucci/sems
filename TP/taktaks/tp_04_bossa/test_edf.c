#include "user_Linux.h"
#include "user_EDF.h"
#include "user_Fixed_priority.h"

main () {
  int prop, edfid;

  // Assuming that the only current scheduler is Linux, the following
  // creates a hierarchy with Fixed_priority at the root and Linux
  // (with priority 20) and EDF (with priority 30) as the two children.
  // mount_rot and mount are defined in load.c
  prop = 20;
  mount_root("Fixed_priority",1,&prop);
  prop = 10;
  edfid = mount("Fixed_priority","EDF",1,&prop);

  // Make the priority of EDF greater than that of Linux.
  // This function is defined in user_Fixed_priority.c, which is generated
  // by the Bossa compiler
  Fixed_priority_setImportance(edfid,30);

  // Attach the current process to the EDF scheduler with a reservation of
  // 10 jiffies out of every 20 jiffies
  // This function is defined in user_EDF.c, which is generated
  // by the Bossa compiler
  EDF_attach(0, 10, 20);

  // Reattach the current process to the Linux scheduler
  // This function is defined in user_Linux.c, which is generated
  // by the Bossa compiler
  Linux_attach(0, BOSSA_SCHED_OTHER, 0, 20);

  // Dismantle the hierarchy
  // This function is defined in load.c
  unmount("EDF");
  unmount("Fixed_priority");
}
