//
//  simple_semaphore.c
//  
//
//  Created by Marco on 13/12/2015.
//
//

#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <string.h>


int main(){
    sem_t sema;

    if (sem_init(&sema, 0, 2) == 1 ) {
        perror("semaphore");
    }

    
    int i;
    int temp=1;
    for (i=0;i<5;i++){
        sem_getvalue(&sema,&temp);
        printf("sema value: %d\n",temp);
        sem_wait(&sema);
        printf("semaphore passed\n");
    }
}