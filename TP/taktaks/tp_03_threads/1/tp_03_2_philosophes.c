#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

/* tp sur les threads POSIX */

pthread_mutex_t mutex[5]; // creo un array di mutex (rapprensenteranno le forchette)

int fork_right(int phil) {
  return phil;
}
int fork_left(int phil) {
  return phil != 0 ? phil-1 : 4;
}

char* say(int phil, char* message) {
  printf("philosoph %x: %s\n", phil, message); 
}

void* f( void *arg )
{
  int num = *(int*) arg;
  printf("I'm philosoph: %x\n", num);

  // think

  say(num,"I am thinking");

  // take left fork
  pthread_mutex_lock(&mutex[fork_left(num)]);
  say(num,"I take the fork on my left");
  
  // wait to take the second fork
  sleep(1);

  // take right fork
  pthread_mutex_lock(&mutex[fork_right(num)]);
  say(num,"I take the fork on my right and I eat");


  
  sleep(2);

  pthread_mutex_unlock(&mutex[fork_left(num)]);  
  pthread_mutex_unlock(&mutex[fork_right(num)]);

  
}

main()
{
  pthread_t thread[5];
  int th_id[] = {0,1,2,3,4};
  int i;

  // init mutexes
  for (i=0;i<5;i++) {
    if ( pthread_mutex_init( &mutex[i], NULL ) == 1 ) {
      perror("mutex");
    }
  }


  /* crea i threads */
  for (i=0; i<5; i++) {
    if(pthread_create( &thread[i], NULL, f, (void*) &th_id[i]) )
      {
	fprintf(stderr,"Error - pthread_create()");
	exit(EXIT_FAILURE);
      }
  }

  /* crea i threads */
  for (i=0; i<5; i++) {
    pthread_join( thread[i], NULL);
  }

  exit(EXIT_SUCCESS);
}


