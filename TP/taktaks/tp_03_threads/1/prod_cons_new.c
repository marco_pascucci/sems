//
//  prod_cons_new.c
//
//
//  Created by Marco on 13/12/2015.
//
//



// WARNING : semaphores do not work in mac OS !!!!

/*
 
 each time a produce produces, he fills a buffer
 
 each time a consumer consumes, he frees a buffer
 
 There is a finite number buffers
 
 Two semaphores represent the number of full and empty buffers
 
 producers wait until there are empty buffers
 
 consumers wait until there are full buffers.
 
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <string.h>

#define NUMBER_OF_BUFFERS 10    // number of buffers

// SEMAPHORES

/* Access control to buffers.
 * It's max value will be 1.
 * It is used to control the crytical section
 * it could be done with a simple mutex, but
 * we want to solve this exercice only with semaphores */
sem_t sema_critical;

/* Number of empty buffers.
 * it's highest value can be NUMBER_OF_BUFFERS.
 * it is decreased by one when a product is produced and
 * indreased by one when a product is consumed.*/
sem_t sema_empty;

/* Number of full buffers
 * it's highest value can be NUMBER_OF_BUFFERS.
 * it works exactly in the opposite way than sema_empty */
sem_t sema_full;


// cursors in the buffer.
// the first product is stored in buffer 0.
// the first consumer will look in buffer 0.
int cursor_prod = 0;
int cursor_cons = 0;

/* a product is an integer different from zero.
 * in order to differentiate each product cronologically
 * the integer number will be incremented at each production */
int product = 1;
int max_product=10; // max products to produce before stopping produnction

// the buffers
int buffer[NUMBER_OF_BUFFERS];

// function to produce readable output
void say (char* tp, int n, char* message) {
    printf("%s %x: %s\n", tp, n, message);
}

void * f_producer (void *arg)
{
    /* a producer can produce only if no consumers are consuming
     * in other words writing access to the buffers is allowed only
     * if no proces is reading. */
    
    char *tp = "producer";
    int num = * (int*) arg;  // cast *void into *int and get the value
    
    say(tp,num,"created");
    
    while (product<=max_product) {
        sem_wait(&sema_empty);       // is there an empty buffer
        // (wait to decrement sema_empty)
        
        sem_wait(&sema_critical);    // do I have writing permission ?
        // (waint to decrement sema_critical)
        
        
        buffer[cursor_prod] = product;  // produce
        char *s[64];
        sprintf(s, "produced: %d", product);
        say(tp, num, s);
        
        
        cursor_prod++;                  // increment position in buffer
        cursor_prod = cursor_prod % NUMBER_OF_BUFFERS;   // buffer is cyclical
        //sprintf("%d\n",cursor_prod);
        product++;                      // increment the product
        
        sem_post(&sema_critical);    // release the access lock
        // (increment the semaphore)
        
        sem_post(&sema_full);        // increment the number of full buffers
        // NOTE that sema_empty is not incremented!
        // one empty buffer becomes full.
        sleep(1);
    }
    
    say(tp,num,"stopped production");
}


void * f_consumer (void *arg)
{
    /* a consumer can consume only if no producer is prodcing
     * if products are available it consumes the first available product */
    
    char *tp = "consumer";
    int num = * (int*) arg; // consumer number
    
    say(tp,num,"created");
    
    while (1) {
        //int temp;
        //sem_getvalue(&sema_full,&temp);
        //printf("sema_full: %d\n",temp);
        
        sem_wait(&sema_full);
        sem_wait(&sema_critical);
        
        int consumed_product = buffer[cursor_cons]; // consume product
        cursor_cons++;  // increment product cursor
        cursor_cons = cursor_cons % NUMBER_OF_BUFFERS;
        
        char *s[64];
        sprintf(s, "consumed: %d", consumed_product);
        say(tp, num, s);
        
        sem_post(&sema_critical);
        sem_post(&sema_empty);
        sleep(1);
    }
}



int main()
{
    printf("START\n");
    
    int i;
    
    // all buffers are initialized to zero, since they are initially empty
    for (i = 0; i < NUMBER_OF_BUFFERS; i++){
        buffer[i] = 0;
        //printf("initialized %d to %d\n",i,buffer[i]);
    }
    
    int n_cons = 3; // number of consumers
    int n_prod = 1; // number of producers
    int id_conumers[n_cons];    // id of the consumers
    int id_producers[n_prod];   // id of the producers
    
    pthread_t thread_producer[n_prod];  // array of producers
    pthread_t thread_consumer[n_cons];  // array of consumers
    
    
    // SEMAPHORES INITIALIZATION
    /* If the second argument is 0, this semaphore will be used only
     * by threads which are childs of this process.
     * the third argument is the initial value of the semaphore
     * the min value is 0 by definition.
     */
    if (sem_init(&sema_empty, 0, NUMBER_OF_BUFFERS) == 1 ) {
        perror("semaphore");
    }
    if (sem_init(&sema_full, 0, 0) == 1 ) {
        perror("semaphore");
    }
    /* this is a binary semaphores
     * it will only be 0 or 1
     * we use it instead of a mutex, to protect the
     * critical section */
    if (sem_init(&sema_critical, 0, 1) == 1 ) {
        perror("semaphore");
    }
    
    // THREADS CREATION
    for (i=0; i < n_prod; i++) {
        id_producers[i] = i;
        if(pthread_create( &thread_producer[i], NULL,
                          f_producer, (void*) &id_producers[i]) == 1 )
        {
            fprintf(stderr,"Error - pthread_create()");
            exit(EXIT_FAILURE);
        }
    }
    
    /* crea i threads consumatori*/
    for (i=0; i < n_cons; i++) {
        id_conumers[i] = i;
        if(pthread_create( &thread_consumer[i], NULL,
                          f_consumer, (void*) &id_conumers[i]) == 1)
        {
            fprintf(stderr,"Error - pthread_create()");
            exit(EXIT_FAILURE);
        }
    }
    
    
    // start threads
    for (i=0; i < n_cons; i++) {
        pthread_join( thread_consumer[i], NULL);
    }
    for (i=0; i < n_prod; i++) {
        pthread_join( thread_producer[i], NULL);
    }
}