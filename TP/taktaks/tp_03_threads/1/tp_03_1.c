#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

/* tp sur les threads POSIX */

int g = 0; //global variable
pthread_mutex_t mutex; // il mutex deve essere globale

void *increment( void *ptr )
{
  pthread_mutex_lock(&mutex);
  char *message;
  message = (char *) ptr; // on cast de addresse *void à addresse *char
  printf("%s \n", message);
  g = g + 1;
  sleep(10);
  pthread_mutex_unlock(&mutex);
}

void *decrement( void *ptr )
{
  pthread_mutex_lock(&mutex);
  char *message;
  message = (char *) ptr;
  printf("%s \n", message);
  g = g - 1;
  pthread_mutex_unlock(&mutex);
}

main()
{
  pthread_t thread1, thread2;
  const char *message1 = "Thread 1";
  const char *message2 = "Thread 2";
  int  iret1, iret2;
  
  if ( pthread_mutex_init( &mutex, NULL ) == 1 ) {
    perror("mutex");
  }


  /* Create independent threads each of which will execute function */
  iret1 = pthread_create( &thread1, NULL, increment, (void*) message1);
  if(iret1)
  {
    fprintf(stderr,"Error - pthread_create() return code: %d\n",iret1);
    exit(EXIT_FAILURE);
  }

  iret2 = pthread_create( &thread2, NULL, decrement, (void*) message2);
  if(iret2)
  {
     fprintf(stderr,"Error - pthread_create() return code: %d\n",iret2);
     exit(EXIT_FAILURE);
  }

  //printf("pthread_create() for thread 1 returns: %d\n",iret1);
  //printf("pthread_create() for thread 2 returns: %d\n",iret2);

  /* Wait till threads are complete before main continues. Unless we  */
  /* wait we run the risk of executing an exit which will terminate   */
  /* the process and all threads before the threads have completed.   */

  pthread_join( thread1, NULL);
  pthread_join( thread2, NULL); 

  printf("g: %x\n", g);
  exit(EXIT_SUCCESS);
}


