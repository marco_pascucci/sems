#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <string.h>



/* un semaforo permette di limitare il numero di processi che accedono alla stessa risorsa */

pthread_mutex_t mutex_critic;
pthread_mutex_t mutex_write_lock;

sem_t semaphore;

char *list_s[6] = {"uno","due","tre","quattro","cinque","\n"};
char *buffer = "";

char* say(char* tp, int n, char* message) {
  printf("%s %x: %s\n", tp, n, message); 
}

int max_simultaneus_readers = 3;

// lecteur
void *f_cons( void *arg ) {
  
  char *tp = "consumatore";
  int num = *(int*) arg;
  int nb_lecteurs; // number of lectors
  char *s;
	char olds[32];

  say(tp, num, "eccomi!");

  while (1) {


  // lock per entrare in sezione critica 
  pthread_mutex_lock(&mutex_critic);
  
  sem_getvalue(&semaphore, &nb_lecteurs);
  // se il numero di lettori (dato da un semaforo) == 0
  // lock la scrittura (non si puo piu scrivere)
  if (nb_lecteurs == max_simultaneus_readers) {
    pthread_mutex_lock(&mutex_write_lock);
  }

  // incrementa il numero di lettori
  //sem_post(semaphore);

  // rilascia il lock della sezione critica
  pthread_mutex_unlock(&mutex_critic);

  // chiede un tocken al semaforo
	//say(tp, num, "fermo al semaforo");
  sem_wait(&semaphore);
	//say(tp, num, "passato il semaforo");

  // effettua la lettura
  s = buffer;
    

  // decrementa il numero di lettori
  //sem

  // se il numero di lettori è zero, rilascia la scrittura
  sem_getvalue(&semaphore, &nb_lecteurs);
	//char *temp_s[32];
	//sprintf(temp_s, "%d", nb_lecteurs);
	//say(tp, num, temp_s);
  if (nb_lecteurs == max_simultaneus_readers) {
    pthread_mutex_unlock(&mutex_write_lock);
  }

	if (s=="\n") break;

	if (strcmp(s,olds)) {
  		say(tp, num, s);
		sprintf(olds,s);
	}

  //return ( void *) s;
  }
}



// WRITER
void *f_prod( void *arg ) {
  
  int i;
  char *tp = "produttore";
  int num = *(int*) arg;
	char *temp_s;


  say(tp, num, "eccomi!");
 
  for (i=0; i<6; i++) {
    // lock scrittura
    pthread_mutex_lock(&mutex_write_lock);
    // NOTA: il lock della sezione critica non è necessario 

    // scrivi
    buffer = list_s[i];
		say(tp, num, list_s[i]);

    // unlock scrittura
    pthread_mutex_unlock(&mutex_write_lock);
    sleep(1);
  }
  
}

void *main()
{
  int th_id[] = {0,1,2,3,4};
  int i;
  int n_cons = 3; //numero di consumatori
  int n_prod = 2; //numero di produttori

  pthread_t thread_produttore[n_prod];
  pthread_t thread_consumatore[n_cons];


  // init mutexes
  if ( pthread_mutex_init( &mutex_critic, NULL ) == 1 ) {
    perror("mutex");
  }
  if ( pthread_mutex_init( &mutex_write_lock, NULL ) == 1 ) {
    perror("mutex");
  }

  /* crea il semaforo */
  if (sem_init(&semaphore, 0, max_simultaneus_readers) == 1 ) {
    perror("semaphore");
  }

  /* crea i threads produttori */
  for (i=0; i<n_prod; i++) {
    if(pthread_create( &thread_produttore[i], NULL, f_prod, (void*) &th_id[i]) )
      {
        fprintf(stderr,"Error - pthread_create()");
        exit(EXIT_FAILURE);
      }
	}

	/* crea i threads consumatori */
  for (i=0; i<n_cons; i++) {
    if(pthread_create( &thread_consumatore[i], NULL, f_cons, (void*) &th_id[i]) )
      {
        fprintf(stderr,"Error - pthread_create()");
        exit(EXIT_FAILURE);
      }
  }





  /* join threads */
  for (i=0; i<5; i++) {
    pthread_join( thread_produttore[i], NULL);
    pthread_join( thread_consumatore[i], NULL);
  }

  exit(EXIT_SUCCESS);
}

