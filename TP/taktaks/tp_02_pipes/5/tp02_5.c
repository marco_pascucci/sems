#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h> // error code: man 7 signal



int pip[2];
int pid;

/* vediamo come si cread un HANDLER per un errore
 * questo handler gestisce l'errore SIGPIPE.
 */
void handler_SIGPIPE(int s)
{
  char* who;
  if (pid > 0) 
  {
    who = "PERE";
  } else {
    who = "FILS";
  }
  printf("%s: SIGPIPE, signal %d reçu\n", who, s);
}

/* questo secondo handler gestisce il segnale SIGCHLD
 * emesso quando un processo figilo termina.
 * Di solito si implementa solo una sola funzione handler
 * che esegue istruzioni diverse a seconda del tipo di
 * errore indicato in argomento (int s).
*/
void handler_SIGCHLD(int s)
{  
  char* who;
  if (pid > 0) 
  {
    who = "PERE";
  } else {
    who = "FILS";
  }
  printf("%s: SIGCHLD, signal %d reçu\n", who, s);
  wait();
  /* aspetta che il figlio sia terminato e raccoglie le informazioni associate
    evitando che il figlio resti nello stato ZOMBIE.
    Lo mettiamo qui cosi' il padre non è obbligato ad attendere inutilmente
    raccoglie i dati (e chiude il figlio) solo quando il figlio è terminato */
}

int main()
{
    int nb_ecrit;
    
    /* per prima cosa connettiamo gli errori alla fuinzione handler.
     * Senza signal(), l'esecuzione del codice si interrompe,
     * senza mostrare alcun errore, perché è il comportamento
     * di default per l'errore SIGPIPE. */

    signal(SIGPIPE, handler_SIGPIPE); //Broken pipe: write to pipe with no readers

    signal(SIGCHLD, handler_SIGCHLD);
    
    /* ouverture d'un pipe */
    if(pipe(pip))
    { perror("pipe");
        exit(1);}
    
    /* fork */
    pid = fork();
    
    if (pid == 0) // SON
    {
        close(pip[0]);
        close(pip[1]);
        printf("FILS: Je suis le fils, et je meur ici!\n");
        exit(1);
    }
    else            // DAD
    {
        close(pip[0]);
        for(;;){
            /* Upon successful completion, write() and pwrite()
	       shall return the number of bytes actually written
	       to the file associated with fildes, -1 otherwise */
            if ((nb_ecrit = write(pip[1], "ABC", 3)) == -1)
            {
	        perror ("pb write");
                exit(1);
            }
            else
                printf ("PERE: je suis pere et viens d'ecrire %d bytes dans le buffer du pipe\n", nb_ecrit);
	    printf("PERE: j'attends une segonde...\n");
	    sleep(1);
        }
    }
}
