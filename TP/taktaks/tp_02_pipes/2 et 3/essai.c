#include <unistd.h>
#include <stdio.h>

main()
{
  int pid, mypid, fatherpid, son_pid;
  pid = fork(); // duplicate the process
  if (pid == 0) {
    for(;;){
      printf("je suis fils\n");
      mypid = getpid();
      fatherpid = getppid();
      printf("mon PID est %d\n",mypid);
      printf("le PID de mon père est %d\n",fatherpid);
      printf("je n'ai pas de fils");
//      execl("/bin/ls","ls","-l","/",NULL);
	  printf("\n\n");
	  printf("Liste des processus actifs\n");
	  system("/bin/ps");
	  printf("\n");
	  execl("/bin/echo","echo","fin du processus fils",NULL);
//	  execl("/bin/ps","ps","-s",NULL);
    }
  }else{ 
    printf("je suis père\n");
    mypid = getpid();
    fatherpid = getppid();
    printf("mon PID est %d\n",mypid);
    printf("le PID de mon père est %d\n",fatherpid);
    printf("le PID de mon fils est %d\n",pid);
    printf("\n");
    wait();
    printf("\n");
	printf("fin du processus père\n");

  }
}

// to show the process table: ps ax -1 | grep testfork.o
/* 
 5778 pts/2    S+     0:00 ./testfork.o
 5779 pts/2    S+     0:00 ./testfork.o
 5781 pts/3    S+     0:00 grep --color=auto testfork
*/
