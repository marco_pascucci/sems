#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>

int pip[2];
int main()
{
    int fd1[2], fd2[2];  // file descriptor for the PIPELINE
                        // fd[0] is for reading , fd[1] is writing
    int nbytes;
    int childpid;
    char readbuffer[80];
    char *writestring;
    
    /* opening pipelines */
    if (pipe(fd1))
    {
        perror("pipe");
        exit(1);
    }
    
    if (pipe(fd2))
    {
        perror("pipe");
        exit(1);
    }
    
    /* forking the process */
    if ((childpid = fork()) == -1)
    {
        perror("fork");
        exit(1);
    }
    
    /* code for child and father */
    if (childpid == 0)
    {
        close(fd1[1]);    // the child writes on 1
        close(fd2[0]);    // the child reads on 2
        
        /* write */
        writestring = "hello, je suis le processus B (fils)";
        write(fd2[1], writestring, (strlen(writestring)+1));
        
        /* read */
        nbytes = read(fd1[0],readbuffer,sizeof(readbuffer));
        printf("B recieved: %s\n",readbuffer);
        
    } else {
        close(fd1[0]);    // father does the opposite
        close(fd2[1]);
        
        /* write */
        writestring = "hello, je suis le processus A (père)";
        write(fd1[1], writestring, (strlen(writestring)+1));
	// il (+1) invia il carattere terminale per indicare che
	// l'array di caratteri è terminato

        
        /* read */
        nbytes = read(fd2[0],readbuffer,sizeof(readbuffer));
        printf("A recieved: %s\n",readbuffer);
    }
        
    
}
