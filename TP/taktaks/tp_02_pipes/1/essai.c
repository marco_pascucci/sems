#include <unistd.h>
#include <stdio.h>
main()
{
  int pid;
  pid = fork(); // duplicate the process
  if (pid == 0) {
    for(;;){
      printf("je suis fils\n");
      execl("/bin/ls","ls","-l","/",NULL);
    }
  }else{ 
    printf("je suis père\n");
    wait();
  }
}

// to show the process table: ps ax -1 | grep testfork.o
/* 
 5778 pts/2    S+     0:00 ./testfork.o
 5779 pts/2    S+     0:00 ./testfork.o
 5781 pts/3    S+     0:00 grep --color=auto testfork
*/
