#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>

int fd;
char* readbuff[64];

int main()
{
    int nb_ecrit;
    int pid;
    
    /* creation d'un fifo */
    if(mkfifo("myfifo",00777 ))
    { 
      perror("myfifo");
    }

    if ( (fd = open("myfifo",O_RDWR)) == -1 ) {
      perror("open myfifo");
    }
    // write on pipe
    write(fd,"test",4);

    // read in pipe
    read(fd,readbuff, sizeof(readbuff));
    // fd can not be read again, the information is deleted upon lecture

    close(fd);

    printf("read: %s\n", readbuff);
}
