# include <stdio.h>
# include <unistd.h>

int est_chifre(char c){
    if ( c >= '0' && c <= '9' ) {
        return 1;
    } else {
        return 0;
    }
}

int main() {
    char c;
    
    printf("input one char: ");
    c = getchar();
    
    printf("entered char: %c\n",c);
    printf("number? %d\n", est_chifre(c));
}
