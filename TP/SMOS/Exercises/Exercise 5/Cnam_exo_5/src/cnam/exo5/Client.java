package cnam.exo5;

import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;

import com.licel.jcardsim.base.Simulator;
import com.licel.jcardsim.io.JavaCardInterface;
import com.licel.jcardsim.smartcardio.JCSCard;
import com.licel.jcardsim.smartcardio.JCSCardTerminals;

import javacard.framework.AID;


public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Simulator simulator = new Simulator();

		byte[] appletAIDBytes = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
		AID appletAID1 = new AID(appletAIDBytes, (short) 0, (byte) appletAIDBytes.length);

		simulator.installApplet(appletAID1, Applet_1.class);

		// Select applet 1
		simulator.selectApplet(appletAID1);		
				

		System.out.println("SET KEY");
		
		
		byte[] response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x02 ,0x00 , 0x00 , 0x08 , 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08});
		System.out.print("Expected : 9000");    System.out.println(" --> Result :" + byteArrayToHex(response));
		
		System.out.println("DECRYPT :");
		
		response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x04 ,0x00 , 0x00 , 0x20 , 
				 0x3b, 0x00, 0x56, (byte)0xb6, (byte)0xc2, (byte)0xf6, 0x38, 0x79,
				 0x41, (byte)0xce, 0x3a, (byte)0xe1, (byte)0xec, (byte)0xb5, 0x16, (byte)0xab, 
				 (byte)0xf0, (byte)0xbb, 0x39, 0x5e, (byte)0xcd, 0x64, 0x79, 0x3f, 
				 0x37, (byte)0xd0, 0x09, (byte)0xff, 0x46, 0x76, 0x36, 0x66		
					});
		
				
		System.out.println(hexToASCII(byteArrayToHex(response)));
		
	}
	
	
	public static String byteArrayToHex(byte[] a) {
		   StringBuilder sb = new StringBuilder(a.length * 2);
		   for(byte b: a)
		      sb.append(String.format("%02x", b & 0xff));
		   return sb.toString();
		}

	
	public static String hexToASCII(String hexValue)
	{
	    StringBuilder output = new StringBuilder("");
	    for (int i = 0; i < hexValue.length(); i += 2)
	    {
	        String str = hexValue.substring(i, i + 2);
	        output.append((char) Integer.parseInt(str, 16));
	    }
	    return output.toString();
	}
	
}
