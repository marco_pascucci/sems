package cnam.exo3;

import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;

import com.licel.jcardsim.base.Simulator;
import com.licel.jcardsim.io.JavaCardInterface;
import com.licel.jcardsim.smartcardio.JCSCard;
import com.licel.jcardsim.smartcardio.JCSCardTerminals;

import javacard.framework.AID;


public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Simulator simulator = new Simulator();

		byte[] appletAIDBytes = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
		AID appletAID1 = new AID(appletAIDBytes, (short) 0, (byte) appletAIDBytes.length);

		simulator.installApplet(appletAID1, Applet_1.class);

		// Select applet 1
		simulator.selectApplet(appletAID1);		
		
//		byte[] response = simulator.transmitCommand(new byte[]{(byte)0xB0, 0x00, 0x00, 0x00});
//		System.out.println(byteArrayToHex(response));

		
		
//		
//		

		System.out.println("Read counter value (wrong class)");
		
		
		byte[] response = simulator.transmitCommand(new byte[]{(byte)0x40, 0x04 ,0x00 , 0x00 , 0x02	});
		System.out.print("Expected : 6e00");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Read counter value (wrong P1)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x04 ,0x07 , 0x00 , 0x02	});
		System.out.print("Expected : 6A86");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Read counter value (wrong P2)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x04 , 0x00 , 0x07 , 0x02	});
		System.out.print("Expected : 6A86");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Read counter value (wrong Le)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x04 ,0x00 , 0x00 , 0x04	});
		System.out.print("Expected : 6C02");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Read counter value (ok)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x04 ,0x00 , 0x00 , 0x02  	});
		System.out.print("Expected : 00009000"); System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("");
		System.out.println(" Test command 'Set Counter'");
		System.out.println("");

		System.out.println("Initialize the counter to 255 (wrong class)");
 response = simulator.transmitCommand(new byte[]{(byte)0x40, 0x02 ,0x00 , 0x00 , 0x02 , 0x00, (byte) 0xFF	});
		System.out.print("Expected : 6e00");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Initialize the counter to 255 (wrong P1)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x02 ,0x07 , 0x00 , 0x02 , 0x00, (byte) 0xFF	});
		System.out.print("Expected : 6A86");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Initialize the counter to 255 (wrong P2)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x02 ,0x00 , 0x07 , 0x02 , 0x00, (byte) 0xFF	});
		System.out.print("Expected : 6A86");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Initialize the counter to 255 (wrong Lc)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x02 , 0x00 , 0x00 , 0x01 , (byte) 0xFF	});
		System.out.print("Expected : 6700");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Initialize the counter to 255 (ok)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x02 , 0x00 , 0x00 , 0x02 , 0x00, (byte) 0xFF	});
		System.out.print("Expected : 9000");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("");
		System.out.println(" Test command 'Inc Counter'");
		System.out.println("");

		System.out.println("Incremente the counter of 1 (wrong class)");
 response = simulator.transmitCommand(new byte[]{(byte)0x40, 0x06 , 0x00 , 0x00 , 0x01 , 0x01	});
		System.out.print("Expected : 6E00");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Incremente the counter of 1 (wrong P1)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x06 , 0x07 , 0x00 , 0x01 , 0x01	});
		System.out.print("Expected : 6A86");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Incremente the counter of 1 (wrong P2)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x06 , 0x00 , 0x07 , 0x01 , 0x01	});
		System.out.print("Expected : 6A86");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Incremente the counter of 1 (wrong Lc)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x06 , 0x00 , 0x00 , 0x02 , 0x00, 0x01	});
		System.out.print("Expected : 6700");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Incremente the counter of 1 (ok)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x06 , 0x00 , 0x00 , 0x01 , 0x01	});
		System.out.print("Expected : 9000");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("");
		System.out.println(" Test command 'Dec Counter'");
		System.out.println("");

		System.out.println("Decremente the counter of 1 (wrong class)");
 response = simulator.transmitCommand(new byte[]{(byte)0x40, 0x08 , 0x00 , 0x00 , 0x01 , 0x01	});
		System.out.print("Expected : 6E00");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Decremente the counter of 1 (wrong P1)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x08 , 0x07 , 0x00 , 0x01 , 0x01	});
		System.out.print("Expected : 6A86");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Decremente the counter of 1 (wrong P2)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x08 , 0x00 , 0x07 , 0x01 , 0x01	});
		System.out.print("Expected : 6A86");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Decremente the counter of 1 (wrong Lc)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x08 , 0x00 , 0x00 , 0x02 , 0x00, 0x01	});
		System.out.print("Expected : 6700");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Decremente the counter of 1 (ok)");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x08 , 0x00 , 0x00 , 0x01 , 0x01	});
		System.out.print("Expected : 9000");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("");
		System.out.println(" Functionality tests");
		System.out.println("");

		System.out.println("Initialize the counter to 255");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x02 , 0x00 , 0x00 , 0x02 , 0x00, (byte) 0xFF	});
		System.out.print("Expected : 9000");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Read counter value");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x04 , 0x00 , 0x00 , 0x02	});

		System.out.print("Expected : 00FF9000"); System.out.println(" --> Result :" + byteArrayToHex(response));
 

		System.out.println("Decremente the counter of 10");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x08 , 0x00 , 0x00 , 0x01 , 0x0A	});
		System.out.print("Expected : 9000");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Read counter value");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x04 , 0x00 , 0x00 , 0x02	});
		System.out.print("Expected : 00F59000"); System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Incremente the counter of 100");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x06 , 0x00 , 0x00 , 0x01 , 0x64	});
		System.out.print("Expected : 9000");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Read counter value");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x04 , 0x00 , 0x00 , 0x02});
		System.out.print("Expected : 01599000"); System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("");
		System.out.println(" Sign tests : 0x7FFF => 0x8000");
		System.out.println("");

		System.out.println("Initialize the counter to 32767");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x02 , 0x00 , 0x00 , 0x02 , 0x7F, (byte) 0xFF	});
		System.out.print("Expected : 9000");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Incremente the counter of 1");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x06 , 0x00 , 0x00 , 0x01 , 0x01	});
		System.out.print("Expected : 9000");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Read counter value");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x04 , 0x00 , 0x00 , 0x02	});
		System.out.print("Expected : 80009000"); System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Incremente the counter of 1");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x06 , 0x00 , 0x00 , 0x01 , 0x01	});
		System.out.print("Expected : 9000");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Read counter value");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x04 , 0x00 , 0x00 , 0x02	});
		System.out.print("Expected : 80019000"); System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("");
		System.out.println(" Overflow test : 0xFFFF => 0x0000");
		System.out.println("");

		System.out.println("Initialize the counter to -1");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x02 , 0x00 , 0x00 , 0x02 , (byte) 0xFF, (byte) 0xFF	});
		System.out.print("Expected : 9000");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Incremente the counter of 1");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x06 , 0x00 , 0x00 , 0x01 , 0x01	});
		System.out.print("Expected : 9000");    System.out.println(" --> Result :" + byteArrayToHex(response));

		System.out.println("Read counter value");
 response = simulator.transmitCommand(new byte[]{(byte)0x80, 0x04 , 0x00 , 0x00 , 0x02	});
		System.out.print("Expected : 00009000"); System.out.println(" --> Result :" + byteArrayToHex(response));
		
		
		
		
		
	}
	
	
	public static String byteArrayToHex(byte[] a) {
		   StringBuilder sb = new StringBuilder(a.length * 2);
		   for(byte b: a)
		      sb.append(String.format("%02x", b & 0xff));
		   return sb.toString();
		}

}
