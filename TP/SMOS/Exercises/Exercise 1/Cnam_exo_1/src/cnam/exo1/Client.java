package cnam.exo1;

import com.licel.jcardsim.base.Simulator;
import com.licel.jcardsim.io.JavaCardInterface;
import com.licel.jcardsim.smartcardio.JCSCard;
import com.licel.jcardsim.smartcardio.JCSCardTerminals;

import javacard.framework.AID;


public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Simulator simulator = new Simulator();

		byte[] appletAIDBytes = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
		AID appletAID1 = new AID(appletAIDBytes, (short) 0, (byte) appletAIDBytes.length);

		simulator.installApplet(appletAID1, Applet_1.class);


		
		byte[] appletAIDBytes2 = new byte[]{2, 2, 3, 4, 5, 6, 7, 8, 9};
		AID appletAID2 = new AID(appletAIDBytes2, (short) 0, (byte) appletAIDBytes2.length);
		
		simulator.installApplet(appletAID2, Applet_2.class);
		
		
		// Select applet 1 then 2
		simulator.selectApplet(appletAID1);		
		simulator.selectApplet(appletAID2);
	

	}
	
	
	public static String byteArrayToHex(byte[] a) {
		   StringBuilder sb = new StringBuilder(a.length * 2);
		   for(byte b: a)
		      sb.append(String.format("%02x", b & 0xff));
		   return sb.toString();
		}

}
