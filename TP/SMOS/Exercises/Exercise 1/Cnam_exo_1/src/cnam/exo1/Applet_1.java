package cnam.exo1;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISOException;


public class Applet_1 extends Applet {
		
	public Applet_1() {}
	
	public boolean select() {
		System.out.println("1: select");
		return false;
	}
	
	public void deselect() {
		System.out.println("1: deselect");
	}
	
	public static void install(byte bArray[], short bOffset, byte bLength)
			throws ISOException {
		System.out.println("1: install");
		new Applet_1().register();
		System.out.println("1: object creation");
	}

	@Override
	public void process(APDU arg0) throws ISOException {
		if (!selectingApplet())
			System.out.println("1: process");
	}	
}