package cnam.exo1;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISOException;


public class Applet_2 extends Applet {
		
	public Applet_2() {}
	
	public boolean select() {
		System.out.println("2: select");
		return false;
	}
	
	public void deselect() {
		System.out.println("2: deselect");
	}
	
	public static void install(byte bArray[], short bOffset, byte bLength)
			throws ISOException {
		System.out.println("2: install");
		new Applet_2().register();
		System.out.println("2: object creation");
	}

	@Override
	public void process(APDU arg0) throws ISOException {
		// questa condizione permette di verificare se
		// il metodo eè eseguito in seguito a un select
		// o meno
		if (!selectingApplet())
			System.out.println("2: process");
	}
}