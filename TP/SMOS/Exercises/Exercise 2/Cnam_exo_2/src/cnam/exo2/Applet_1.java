package cnam.exo2;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;

public class Applet_1 extends Applet {

	public short[] ar1;
	public short[] ar2;

	public Applet_1() {
		short size = 1;
		short initValue = 9;

		ar1 = JCSystem.makeTransientShortArray(size, JCSystem.CLEAR_ON_DESELECT);
		ar2 = JCSystem.makeTransientShortArray(size, JCSystem.CLEAR_ON_RESET);

		ar1[0] = initValue;
		ar2[0] = initValue;

		System.out.println("\tApplet_1()");
		System.out.println("\t\tar1 CLEAR_ON_DESELECT: " + String.valueOf(ar1[0]));
		System.out.println("\t\tar2 CLEAR_ON_RESET: " + String.valueOf(ar2[0]));
	}

	/*
	 * questo metodo statico è chiamato dal simulatore è questo stesso metodo
	 * statico che crea l'oggetto di questa classe è chiamato dalla virtual
	 * machine, che tiene traccia del pointer a questo oggetto.
	 */
	public static void install(byte bArray[], short bOffset, byte bLength) throws ISOException {
		Applet_1 applet = new Applet_1();
		applet.register();

		System.out.println("\tinstall()");
		System.out.println("\t\tar1 CLEAR_ON_DESELECT: " + String.valueOf(applet.ar1[0]));
		System.out.println("\t\tar2 CLEAR_ON_RESET: " + String.valueOf(applet.ar2[0]));
	}

	public boolean select() {

		System.out.println("\tselect()");
		System.out.println("\t\tar1 CLEAR_ON_DESELECT: " + String.valueOf(ar1[0]));
		System.out.println("\t\tar2 CLEAR_ON_RESET: " + String.valueOf(ar2[0]));

		return true;
	}

	public void process(APDU arg0) throws ISOException {
		if (!selectingApplet()) {
			System.out.println("\tprocess()");
			System.out.println("\t\tar1 CLEAR_ON_DESELECT: " + String.valueOf(ar1[0]));
			System.out.println("\t\tar2 CLEAR_ON_RESET: " + String.valueOf(ar2[0]));
		}
	}

	public void deselect() {
		System.out.println("\tdeselect()");
		System.out.println("\t\tar1 CLEAR_ON_DESELECT: " + String.valueOf(ar1[0]));
		System.out.println("\t\tar2 CLEAR_ON_RESET: " + String.valueOf(ar2[0]));
	}
}
