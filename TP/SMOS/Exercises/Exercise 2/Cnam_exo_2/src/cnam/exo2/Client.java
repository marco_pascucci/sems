package cnam.exo2;

import com.licel.jcardsim.base.Simulator;
import com.licel.jcardsim.io.JavaCardInterface;
import com.licel.jcardsim.smartcardio.JCSCard;
import com.licel.jcardsim.smartcardio.JCSCardTerminals;

import javacard.framework.AID;


public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Simulator simulator = new Simulator();

		byte[] appletAIDBytes = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
		AID appletAID1 = new AID(appletAIDBytes, (short) 0, (byte) appletAIDBytes.length);

		System.out.println("Initialization");
		
		simulator.installApplet(appletAID1, Applet_1.class);

		System.out.println("Applet deselect");

		simulator.selectApplet(appletAID1);
			
		System.out.println("Card reset");
		
		simulator.reset();
		simulator.selectApplet(appletAID1);		
			
	}
	
	
	public static String byteArrayToHex(byte[] a) {
		   StringBuilder sb = new StringBuilder(a.length * 2);
		   for(byte b: a)
		      sb.append(String.format("%02x", b & 0xff));
		   return sb.toString();
		}

}
